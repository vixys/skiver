
(function (root, factory) {

    /* istanbul ignore next */
    if (typeof define === 'function' && define.amd) {
        define(['skiver'], function (_) {
            return (factory(root, Sk));
        });
    } else {
        factory(root, root.Sk);
    }

}(this, function (root, Sk) {
    'use strict';
    
    Sk.data.loading.msg.modalText = 'Processando';
    Sk.data.loading.msg.contentText = 'Carregando';
    Sk.data.loading.msg.formSaveText = 'Processando Dados';
    Sk.data.validate.msg.error = {
        'required': 'Campo Obrigatório',
        'email': 'Informe um e-mail válido'
    };
    Sk.data.exception.msg.errorTitle = 'Erro encontrado';
    Sk.data.exception.msg.httpErrors = {
        0: 'Ocorreu um erro interno',
        400: 'Acesso não autorizado',
        401: 'Acesso não autorizado',
        403: 'Permissão negada',
        404: 'A página solicitada não foi encontrada'
    };
    Sk.data.popup.msg.imageViewTitle = 'Visualização de Imagem';
    Sk.data.popup.msg.htmlViewTitle = 'Visualização de HTML';
    Sk.data.popup.msg.infoViewTitle = 'Informação';
    Sk.data.popup.msg.confirmValidation = 'O campo de descrição é obrigatório';
    Sk.data.popup.msg.confirmViewTitle = 'Confirmação';
    Sk.data.popup.msg.confirmViewMsg = 'Deseja realmente excluir esse registro?';
    Sk.data.popup.msg.confirmActionMsg = 'Confirmar';
    Sk.data.popup.msg.cancelActionMsg = 'Cancelar';
    Sk.data.popup.msg.close = 'Fechar';
    
}));
