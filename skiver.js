//     Skiver.js

(function (root, factory) {

    /* istanbul ignore next */
    if (typeof define === 'function' && define.amd) {
        define(['underscore', 'jquery'], function (_) {
            return (root.Sk = factory(root, _, $));
        });
    } else {
        root.Sk = factory(root, root._, root.$);
    }

}(this, function (root, _, $) {
    'use strict';

    // Baseline setup
    // --------------

    // Save the previous value of the `Sk` variable.
    var previousInstance = root.Sk;

    // Create a safe reference to the Skiver object for use below.
    var Sk = {
        absolutePath: '/',
        imgPath: 'img/',
        applyAnimations: true,
        AllowSpa: false,
        events: {}
    };

    Sk.VERSION = '1.3';

    // Run Skiver.js in *noConflict* mode, returning the `s` variable to its
    // previous owner. Returns a reference to the Skiver object.
    Sk.noConflict = function () {
        root.Sk = previousInstance;
        return this;
    };

    // A configurable set of messages, templates

    // Sample Data and Templates
    // -----------------
    Sk.data = {
        loading: {
            html: {
                img: [
                    'loading-modal.gif',
                    'loading-central.gif',
                    'loading-local.gif'
                ],
                ico: [
                    'fa fa-spinner fa-pulse fa-2x p-2',
                    'fa fa-spinner fa-pulse',
                    'fa fa-refresh fa-spin'
                ],
                div: [
                    '<div id="{id}" class="sk-loading-modal" style="display: none;"><div class="modal"><div><i class="{ico}"></i><br><span class="text">{text}</span></div></div></div>',
                    '<div id="{id}" class="sk-loading-central"><i class="{ico}"></i></div>',
                    '<span id="{id}" class="sk-loading-local"><i class="{ico}"></i></span>'
                ]
            },
            msg: {
                modalText: '',
                contentText: '',
                formSaveText: ''
            }
        },
        popup: {
            html: {
                proxies: {
                    bootstrap3: {
                        close: '<a href="" class="btn btn-default modal-close">{close}</a>',
                        ok: '<a href="" class="btn btn-default modal-close">{ok}</a>',
                        div: '<div class="modal fade sk-popup" id="sk-popup-{id}" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"></div><div class="modal-footer"></div></div></div></div>',
                        img: '<img src="{img}"><br><br>{msg}',
                        html: '<div class="col-12 col-xs-12 clearfix">{html}</div>',
                        confirmMessage: '{msg}<textarea id="modal-confirm-message" rows="3" class="form-control">{val}</textarea>',
                        confirmActions: '<a href="" class="btn btn-sm btn-danger confirm"></a><a href="" class="btn btn-sm btn-default cancel"></a>'
                    },
                    bootstrap4: {
                        close: '<a href="" class="btn btn-default modal-close">{close}</a>',
                        ok: '<a href="" class="btn btn-default modal-close">{ok}</a>',
                        div: '<div class="modal fade sk-popup" id="sk-popup-{id}" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"></div><div class="modal-footer"></div></div></div></div>',
                        img: '<img src="{img}"><br><br>{msg}',
                        html: '<div class="col-12 col-xs-12 clearfix">{html}</div>',
                        confirmMessage: '{msg}<textarea id="modal-confirm-message" rows="3" class="form-control">{val}</textarea>',
                        confirmActions: '<a href="" class="btn btn-sm btn-danger confirm"></a><a href="" class="btn btn-sm btn-default cancel"></a>'
                    }
                }
            },
            msg: {
                imageViewTitle: 'Image View',
                htmlViewTitle: 'HTML View',
                infoViewTitle: 'Information',
                confirmValidation: 'Field description is required',
                confirmViewTitle: 'Confirmation',
                confirmViewMsg: 'Are you sure about removing it?',
                confirmActionMsg: 'Confirm',
                cancelActionMsg: 'Cancel',
                close: 'Close',
                ok: 'Ok'
            }
        },
        validate: {
            msg: {
                error: {
                    'required': 'Required field',
                    'email': 'Email field'
                }
            }
        },
        exception: {
            msg: {
                httpErrors: {
                    0: 'Internal Error',
                    400: 'Not Authorized',
                    401: 'Not Authorized',
                    403: 'Forbidden',
                    404: 'Not found'
                },
                errorTitle: 'Error'
            }
        }

    };

    // Collection Functions
    // --------------------

    /*
     * Utility for async content manage
     */
    Sk.content = {
        /**
         * Default useful events
         */
        events: {
            apply: function (parent)
            {
                var container = parent ? $(parent) : $('body');

                container.delegate('a[data-sk="1"], button[data-sk="1"]', 'click', Sk.content.events.click);
            },
            click: function (e)
            {
                Sk.preventDefault(e);
                var obj = $(this), data = $(this).data();

                if (!obj.data('url') && obj.is('a')) {
                    obj.data('url', obj.attr('href'));
                    obj.data('history', true);
                }

                Sk.content.load(data);
            }
        },
        /**
         * Load asyncronous data and send response to be interpreted
         */
        get: function (obj, callback)
        {
            var data = typeof obj.data === 'function' ? obj.data() : obj,
                    url = data.url,
                    modalText = data.modalText ? data.modalText : Sk.data.loading.msg.contentText,
                    history = data.history ? true : false,
                    ajaxMethod = 'method' in data ? data.method : 'GET',
                    formData = 'sendData' in data ? data.sendData : {};

            if (!callback && data.callback)
                callback = data.callback;

            if (typeof callback !== 'function')
                eval(data.callback);

            Sk.loading.modal.add(modalText);


            $.ajax({
                url: url,
                data: formData,
                method: ajaxMethod
            }).done(function (response) {
                var result = response;
                try {
                    result = $.parseJSON(response);
                } catch (e) {
                }

                if (typeof result !== 'string') {
                    Sk.request.process(result);
                    Sk.loading.modal.remove();
                } else {
                    Sk.loading.modal.remove();
                    Sk.content.show(result);
                }

                if (typeof callback === 'function')
                    callback();


                if (history)
                    Sk.request.pushHistory(url);
            }).fail(Sk.request.exception.ajaxError);
        },
        /**
         * Go to a URL, but first shows a loading
         */
        location: function (obj)
        {
            var data = typeof obj.data === 'function' ? obj.data() : obj,
                    url = data.url;

            Sk.loading.modal.add(Sk.data.loading.msg.contentText);
            window.location = url;
        },
        /**
         * Loads an anchor asyncronous or not
         */
        load: function (obj, callback)
        {
            var container = $(Sk.contentContainer);

            if (container.length && !!(typeof obj.async === 'undefined' || obj.async) && Sk.AllowSpa)
                return Sk.content.get(obj, callback);
            return Sk.content.location(obj);
        },
        /**
         * Show content received from an async content request
         */
        show: function (html, effect)
        {
            var effect = Sk.applyAnimations,
                    container = $(Sk.contentContainer),
                    loadObj = Sk.loading.modal.add(Sk.data.loading.msg.modalText);
            
            Sk.waitFor(_.partial(function(load) {
                return Sk.loading.modal.isShown(load);
            }, loadObj),
            _.partial( function (effect, container) {
                if (!effect)
                {
                    var children = container.children('div:first');
                    container.html(html);
                    children.trigger('content-loaded');
                    Sk.content.applyShowCallback(children);
                    return;
                }

                container.slideUp(function () {
                    container.html(html)
                            .slideDown(function () {
                                var children = container.children('div:first');
                                children.trigger('content-loaded');
                                Sk.content.applyShowCallback(children);
                            });
                });
            }, effect, container));
        },
        /**
         * Callback executed after displaying content asyncronous received
         */
        applyShowCallback: function (container) {
            if ('showCallback' in Sk.content && typeof Sk.content.showCallback === 'function') {
                Sk.content.showCallback(container);
            }
        },
        showCallback: function (container) {
            Sk.loading.modal.remove();
        }
    };

    /**
     * Handle json responses and server errors
     */
    Sk.request = {
        /**
         * Used to keep address bar updated with async content
         */
        pushHistory: function (url, data) {
            history.pushState({page: url, sk: '1'}, 'sk', url);
        },
        map: {
            'popmsg': function (json) {
                var type = 'type' in json.popmsg ? json.popmsg.type : 'info',
                        position = 'position' in json.popmsg ? json.popmsg.position : 'bottom';
                
                dhtmlx.message.position=position;
                dhtmlx.message({type: "info bg-"+type, text: json.popmsg.text});
            },
            'loading': function (json) {
                Sk.loading.modal.add(json.loading);
            },
            'redirect': function (json) {
                Sk.content.location({url: json.redirect});
            },
            'open': function (json) {
                window.open(json.open);
            },
            'refresh': function (json) {
                Sk.content.location({url: window.location.href});
            },
            'reload': function (json) {
                Sk.content.load({url: window.location.href});
            },
            'load': function (json) {
                Sk.content.load({url: json.load, history: json.history});
            },
            'flow': function (json) {
                $.ajax({
                    url: json.flow,
                    method: 'GET'
                }).done(function (response) {
                    if ('loading' in json) {
                        _.delay(function () {
                            Sk.loading.modal.remove();
                        }, 30);
                    }

                    var result = response;
                    try {
                        result = $.parseJSON(response);
                    } catch (e) {
                    }

                    if (typeof result !== 'string')
                        Sk.request.process(result);

                    if (json.history)
                        Sk.request.pushHistory(json.flow);
                }).fail(Sk.request.exception.ajaxError);
            }
        },
        /**
         * Execute commands received as json from a ajax request
         */
        process: function (json, responseStatus) {
            if (typeof json === 'string') {
                try {
                    json = $.parseJSON(json);
                } catch (e) {
                }

                if (typeof json === 'string') {
                    return;
                }
            }
            
            if (typeof responseStatus === 'undefined')
                responseStatus = 200;

            if (!Object.size(json) && parseInt(responseStatus, 10) !== 200)
                Sk.request.exception.httpError(responseStatus);

            json.history = 'history' in json ? !!json.history : true;

            switch (true) {
                case ('error' in json && json.error !== ''):
                    Sk.popup.simple.info({title: 'Erro encontrado', msg: json.error});
                    break;
                case ('msg' in json && json.msg !== '') || ('info' in json && json.info !== ''):
                    Sk.popup.simple.info({title: 'title' in json ? json.title : '', msg: json.msg || json.info});
                    break;
            }

            _.each(Sk.request.map, function (func, name) {
                if (name in json && json[name] !== '') {
                    func(json);
                }
            }, this);

            if ('callback' in json && json.callback !== '') {
                var callback = eval(json.callback);
                if (typeof callback === 'function') {
                    callback(json, responseStatus);
                }
            }
        },
        exception: {
            show: function (data) {
                data.title = Sk.data.exception.msg.errorTitle;
                Sk.popup.simple.info(data);
            },
            /**
             * Read response and return a json object anyway
             */
            getJSON: function (response) {
                if (!('responseJSON' in response)) {
                    try {
                        response.responseJSON = $.parseJSON(response.responseText);
                    } catch (e) {
                    }
                }

                if (!('responseJSON' in response) || typeof response.responseJSON !== 'object') {
                    response.responseJSON = {};
                }

                return response.responseJSON;
            },
            /**
             * Prepare to read server error and display some info
             */
            ajaxErrorLastCallData: null,
            ajaxError: function (e)
            {
                /* avoid duplicated calls */
                if(e.getAllResponseHeaders()===this.ajaxErrorLastCallData) {
                    return;
                }
                this.ajaxErrorLastCallData = e.getAllResponseHeaders();
                Sk.loading.modal.remove();
                var json = Sk.request.exception.getJSON(e);

                Sk.request.process(json, e.status);
            },
            /**
             * Read server error and trigger show exception
             */
            httpError: function (status)
            {
                var errorNumber = status, msg;
                if (!('status' in Sk.data.exception.msg.httpErrors))
                    errorNumber = 0;

                msg = Sk.data.exception.msg.httpErrors[errorNumber] + ' (' + status + ')';
                Sk.request.exception.show({msg: msg});
            }
        }
    };

    /**
     * Show loading
     * 
     * Images used for displaying loading. (modal, central, local).
     * Modal = loading with dark modal
     * Central = loading in the middle of a container
     * Local = loading before or after a field
     */
    Sk.loading = {
        /**
         * Returns loading object for a container/field
         * @return jquery
         */
        get: function (el)
        {
            var obj = $(el);
            return $('#' + obj.data('sk-loading-id'));
        },
        /** 
         * Remove loading
         */
        remove: function (el)
        {
            var obj = $(el),
                    loadingObj = Sk.loading.get(obj);

            loadingObj.remove();
            obj.data('sk-loading-id', '');
        },
        /**
         * Loading with a dark modal as background
         */
        modal: {
            /**
             * Id
             */
            id: 'sk-loading_modal',
            /**
             * Length of loading modals stacked
             */
            length: 0,
            /**
             * Stack of texts to be shown by modal loading
             */
            texts: [],
            /**
             * Get modal loading object
             */
            get: function ()
            {
                var obj = $('#' + Sk.loading.modal.id);
                return obj.length ? obj : null;
            },
            create: function () {

                var container = $('body'),
                        id = Sk.loading.modal.id,
                        bodyHeight = container.height(),
                        docHeight = $(document).height(),
                        winHeight = $(window).height(),
                        top = winHeight / 2 - 37,
                        html, loadingObj,
                        ico = Sk.data.loading.html.ico[0];

                html = Sk.data.loading.html.div[0].replace('{id}', id)
                        .replace('{ico}', ico)
                        .replace('{text}', '');

                loadingObj = $(html);

                container.append(loadingObj);

                return loadingObj;
            },
            /**
             * Add an item to modal loading stack
             */
            add: function (text) {
                text = text || Sk.data.loading.msg.modalText;
                var loadingObj = Sk.loading.modal.get();

                Sk.loading.modal.texts[Sk.loading.modal.texts.length] = text;
                Sk.loading.modal.length++;

                if (Sk.loading.modal.length === 1) {
                    if (!loadingObj) {
                        loadingObj = Sk.loading.modal.create();
                    }

                    Sk.loading.modal.changeText(loadingObj);
                    Sk.loading.modal.show(loadingObj);
                }
                return loadingObj;
            },
            /**
             * Show modal loading
             */
            show: function (modal) {
                var obj = modal || Sk.loading.modal.get(),
                        effect = Sk.applyAnimations,
                        winHeight = $(window).height(),
                        top = winHeight / 2 - 37;

                obj.css('height', winHeight + 'px');
                $('.modal', obj).css('top', top + 'px');

                if (!effect) {
                    return obj.show();
                }
                return obj.fadeIn();
            },
            isShown: function (modal) {
                if(!Sk.applyAnimations) {
                    return modal.is(':visible');
                }
                return modal.css('opacity').toString() === '1';
            },
            /**
             * Remove loading
             */
            remove: function () {
                if (!Sk.loading.modal.length)
                    return;
                var id = Sk.loading.modal.id, obj, effect;
                if (Sk.loading.modal.length > 1) {
                    Sk.loading.modal.length--;
                    Sk.loading.modal.changeText();
                } else {
                    effect = Sk.applyAnimations;
                    obj = Sk.loading.modal.get();
                    if(!obj) return;

                    if (!effect) {
                        obj.hide();
                        Sk.loading.modal.length--;
                        return;
                    }

                    return obj.fadeOut(200, _.partial(function (length) {
                        Sk.loading.modal.length--;
                        if (!Sk.loading.modal.length) {
                            obj.hide();
                        } else {
                            Sk.loading.modal.changeText()!==false && Sk.loading.modal.show();
                        }
                    }, Sk.loading.modal.length));
                }
            },
            /**
             * Remove visible text and shows the next from stack
             */
            changeText: function (modal)
            {
                if(!Sk.loading.modal.texts.length) {
                    return false;
                }
                
                modal || (modal = Sk.loading.modal.get());

                var id = Sk.loading.modal.id,
                        text = Sk.loading.modal.texts.shift();

                $('span.text', modal).html(text);

                return modal;
            }
        },
        /**
         * Loading in the middle of a container
         */
        central: {
            /**
             * Add central loading to a container
             */
            add: function (el, opt) {
                typeof opt !== 'object' && (opt = {ico: opt ? opt : Sk.data.loading.html.ico[1]});
                var obj = $(el),
                        id = _.uniqueId('sk-loading_'),
                        html = Sk.data.loading.html.div[1].replace('{id}', id).replace('{ico}', opt.ico);
                obj.data('sk-loading-id', id).html(html);
            },
            remove: function (el) {
                var obj = $(el);
                if(!obj.length) return;
                
                var id = obj.data('sk-loading-id');
                
                id && $('#'+id, obj.parent()).remove();
                obj.data('sk-loading-id', null);
            }
        },
        /**
         * Loading before/after a field
         */
        local: {
            /**
             * Add a local loading
             */
            add: function (el, pos, opt) {
                !pos && (pos = 'after');
                typeof opt !== 'object' && (opt = {ico: opt ? opt : Sk.data.loading.html.ico[2]});
                
                var obj = $(el);
                if(!obj.length) return;
                
                var id = _.uniqueId('sk-loading_'),
                        loadingObj, html;

                if (obj.data('sk-loading-id')) {
                    return $('#' + obj.data('sk-loading-id'));
                }

                html = Sk.data.loading.html.div[2].replace('{id}', id).replace('{ico}', opt.ico);
                obj.data('sk-loading-id', id);

                var holder = (/inside/.test(pos) ? obj : obj.parent());
                loadingObj = (/before/.test(pos) ? $(html).prependTo(holder) : $(html).appendTo(holder));
                loadingObj.addClass('sk-loading-pos-'+pos);
                /modal-like/.test(pos) && holder.css('position', 'relative');

                return loadingObj;
            },
            remove: function (el) {
                var obj = $(el);
                if(!obj.length) return;
                
                var id = obj.data('sk-loading-id');
                
                id && $('#'+id, obj.parent()).remove();
                obj.data('sk-loading-id', null);
            }
        }
    };

    /**
     * Management of basic error placement and form send, read server json response, content data load
     */
    Sk.validate = {
        /**
         * CSS class for error placement
         */
        errorClass: "error",
        /**
         * CSS class for error placement
         */
        errorContainerClass: "container-error",
        /**
         * Field holder (trick to avoid problems with input groups)
         */
        inputContainerSelector: ".field",
        running: false,
        init: function (form, rules) {
            if (!form || !form.length)
                return;
            rules = rules || [];

            if (!('submitHandler' in rules)) {
                rules.submitHandler = this.submitHandler;
            }

            rules.showErrors = this.showErrors;
            rules.errorPlacement = this.errorPlacement;
            rules.errorClass = this.errorClass;
            rules.unhighlight = this.unhighlightEvent;
            rules.focusInvalid = _.partial(this._focusInvalid, form);

            form.validate(rules);
            form.bind('after-invalidate-form', this._afterInvalidateForm);
            form.data('sk-ready', 1).trigger('sk-ready');
        },
        addMoreRules: function (form, validation) {
            if (!form.data('sk-ready')) {
                return form.on('sk-ready', _.bind(Sk.validate.addMoreRules, {}, form, validation));
            }

            var validator = form.validate();
            validator.settings.messages = _.extend(validator.settings.messages, validation.messages);
            validator.settings.rules = _.extend(validator.settings.rules, validation.rules);
        },
        /**
         * Submit handler
         */
        submitHandler: function (form)
        {
//            console.log('submithandler called');
            var text = Sk.data.loading.msg.formSaveText, formObj = $(form);
            if (formObj.data('save-text')) {
                text = formObj.data('save-text');
            }

            _.delay(_.partial(function (submitButton, form) {
                var formObj = $(form), dataSend = null;
                if (!Sk.validate.run(formObj)) {
                    return;
                }

                var submit = _.partial(function(formObj, text, submitButton, form) {
                    var dataSend = formObj.serialize();

                if (submitButton.attr('name')) {
                    dataSend += (dataSend === '' ? '' : '&') + submitButton.attr('name') + '=' + submitButton.val();
                }
                if (!!(typeof formObj.data('async') === 'undefined' || formObj.data('async')) && Sk.AllowSpa) {
                    Sk.content.get({
                        modalText: text,
                        url: formObj.attr('action') || window.location.href,
                        sendData: dataSend,
                        method: formObj.attr('method')
                    });
                    return false;
                }

                Sk.loading.modal.add(text);
                form.submit();
                }, formObj, text, submitButton, form);
                
                var files = new Sk.form.ajaxFiles();
                files.sendFiles(form, null, submit);
                
                
            }, $(this.submitButton || null)), 50, form);

            return false;
        },
        /**
         * Validation trigger
         */
        run: function (form)
        {
            var valid, checkBefore = true;
            $('.tab-content > div').css('display', 'block');
            Sk.validate.running = true;

            valid = form.valid();
            $('.tab-content > div').css('display', '');

            if (form.data('check-before-send')) {
                var checkFunction = form.data('check-before-send');
                if (typeof form.data('check-before-send') === 'string') {
                    checkFunction = eval(checkFunction);
                }

                checkBefore = checkFunction();
            }

            return valid && checkBefore;
        },
        /**
         * Implementation of event for after show errors, to walk through tabs and focus first field marked with an error
         */
        showErrors: function (validation) {
            this.defaultShowErrors();
            $(this.currentForm).trigger('after-invalidate-form');
        },
        /**
         * Error placement handler
         */
        errorPlacement: function (error, obj) {
            var containerExists = obj.parents(Sk.validate.inputContainerSelector).length,
                    inputContainer = containerExists ? obj.parents(Sk.validate.inputContainerSelector) : obj;

            error.insertAfter(inputContainer);
            containerExists && inputContainer.addClass(Sk.validate.errorContainerClass);
        },
        /**
         * Unhighlight field handler
         */
        unhighlightEvent: function (element, errorClass, validClass)
        {
            var obj = $(element),
                    tab = obj.parents('.tab-pane:first'),
                    name = obj.attr('name'),
                    containerExists = obj.parents(Sk.validate.inputContainerSelector).length;

            $('[name="' + name + '"]').removeClass(Sk.validate.errorClass);
            containerExists && obj.parents(Sk.validate.inputContainerSelector).removeClass(Sk.validate.errorContainerClass);
//
            if (!$(':input.'+Sk.validate.errorClass, tab).length) {
                Sk.validate._unhighlightTab(null, tab);
            }
        },
        /**
         * Highlight tabs with fields marked, show the first tab with a marked field, focus the first field marked
         */
        _afterInvalidateForm: function ()
        {
            var form = $(this);

            Sk.validate._unhighlightTabs(form);
            Sk.validate._highlightTabsWithError(form);

            if (Sk.validate.running) {
                Sk.validate._focusFirstFieldWithError(form);
            }

            Sk.validate.running = false;
        },
        _focusInvalid: function(form) {
//            console.log('_focusinvalid called');
//            console.log(form);
            Sk.validate._focusFirstFieldWithError(form);
            return true;
        },
        _unhighlightTabs: function (form)
        {
            var tabs = $('.tab-pane', form);

            tabs.each(Sk.validate._unhighlightTab);
        },
        _highlightTabsWithError: function (form)
        {
            var tabs = $('.tab-pane', form),
                    tabsWithError = $(':input:not(:button).error', form).parents('.tab-pane');

            tabsWithError.each(Sk.validate._highlightTab);
        },
        _highlightTab: function (index, element)
        {
            var tab = $(element),
                    link = $('.nav-tabs li a[href="#' + tab.attr('id') + '"]', tab.parents('.tabbable:first'));

            link.addClass('error');
        },
        _unhighlightTab: function (index, element)
        {
            var tab = $(element),
                    link = $('.nav-tabs li a[href="#' + tab.attr('id') + '"]', tab.parents('.tabbable:first'));

            link.removeClass('error');
        },
        _focusFirstFieldWithError: function (form)
        {
//            console.log('_focusFirstFieldWithError called');
            var firstErrorField = $(':input.error', form).eq(0),
                    parentTab = (firstErrorField.parents('.tab-pane:first')),
                    tab = parentTab.lenth ? $('li > a[href=#' + parentTab.attr('id') + ']', $('.nav-tabs', parentTab.parents('.tabbable:first'))) : null,
                    parentAcc = firstErrorField.parents('.dhx_cell_acc.dhx_cell_closed');

            if(tab) {
//                console.log('found tab');
            tab.click();
            tab.parent().click();
            }
            if(parentAcc.length) {
//                console.log('found acc');
                $('> div', parentAcc).click();
            }

//            firstErrorField.focus();
        },
        /**
         * Simplify valitation rules write
         */
        generateRules: function (settings) {
            var result = {rules: {}, messages: {}};
            switch (true) {
                case _.isArray(settings):
                    _.each(settings, function (field) {
                        result.rules[field] = {required: true};
                        result.messages[field] = {};
                        result.messages[field]['required'] = Sk.data.validate.msg.error['required'];
                    });
                    break;
                case _.isObject(settings):
                    _.each(settings, function (format, field) {
                        result.rules[field] = {required: true};
                        if (format !== 'required') {
                            result.rules[field][format] = true;
                        }
                        result.messages[field] = {};
                        result.messages[field]['required'] = format in Sk.data.validate.msg.error ? Sk.data.validate.msg.error['required'] : format;
                        result.messages[field][format] = format in Sk.data.validate.msg.error ? Sk.data.validate.msg.error[format] : format;
                    });
                    break;
            }

            return result;
        }
    };

    Sk.form = {};
    
    function SkFormAjaxFiles () {
        // instance number
        var inum = _.uniqueId('skformajaxfiles');
    /*
         * Armazena id do intervalo do envo de arquivos
         */
        this.sendFilesIndex = 0;

        /*
         * Armazena id do intervalo do envo de arquivos
         */
        this.sendFilesInterval = null;

        /*
         * Armazena funcao de retorno definida pelo usuario
         */
        this.sendFilesCallback = null;

        /*
         * Iframe que executa o envio do formulario
         */
        this.iframe = null;

        /*
         * Container onde estão localizados os campos file
         */
        this.container = null;

        /**
         * Prepara envio arquivos do formulario via HTTP/iframe
         * @param _form formulario que sera enviado
         * @param _el container que recebera o resultado
         * @param _callback funcao que sera executada no retorno da requisicao
         * @access static
         * @return void
         */
        this.sendFiles = function(_form, _el, _callback)
        {
            this.sendFilesCallback = _callback;
            this.iframe = $('<iframe id="'+inum+'" name="'+inum+'" style="display:none;" width="300" height="100" scrolling="auto"></iframe>').appendTo('body');
            this.container = _form;
            
            if($(_form).attr('data-async')==='0') { // form with enctype multipart/form-data that will not be assyncronous
                return this.sentFiles(true);
            }
            
            // remove fields of other attempt to send
            $('input[data-file="1"]', this.container).remove();
            
            this.sendFile();
        };

        /**
         * Prepara envio arquivos do formulario via HTTP/iframe
         * @param _callback funcao que sera executada no retorno da requisicao
         * @access static
         * @return void
         */
        this.sendFile = function()
        {
            var el = $('input:file', this.container).eq(this.sendFilesIndex);

            if(el.length===0) {
                return this.sentFiles(true);
            } else if(!el.val()) {
                this.sendFile(++this.sendFilesIndex);
                return;
            }
            var form = $('<form id="'+inum+'" class="mt-5" name="'+inum+'" target="'+inum+'" style="display:block;" method="post" enctype="multipart/form-data" action="'+$(el).attr('data-action')+'"></form>').appendTo('body');
            $('<div class="w-100 h-100" style="position: absolute;background-color: white;"></div>').appendTo(form);
            $('<input type="text" name="upload[name]" id="upload_name" />').appendTo(form).val($(el).attr('data-name'));
            $('<input type="text" name="upload[dir]" id="upload_dir" />').appendTo(form).val($(el).attr('data-dir'));
            $('<input type="text" name="upload[types]" id="upload_types" />').appendTo(form).val($(el).attr('data-types'));
            $('<input type="text" name="upload[size]" id="upload_size" />').appendTo(form).val($(el).attr('data-size'));

//            console.log({url: $(el).attr('data-action'), name: $(el).attr('data-name'), dir: $(el).attr('data-dir'), types: $(el).attr('data-types'), size: $(el).attr('data-size')});

            el.clone().insertAfter(el);
            var el2 = el.attr('name','uploadfile').appendTo(form);
            form.submit();
            Sk.loading.modal.add('Aguarde... Enviando arquivo '+(this.sendFilesIndex+1)+'/'+$('input:file', this.container).length);
            this.sendFilesInterval = setInterval(_.partial(_.bind(function(form)
            {
        //        sfw.alert('enviando');
                var response = $.trim(($('body > pre',this.iframe.contents()).length>0 ? $('body > pre',this.iframe.contents()) : $('body',this.iframe.contents())).html());
                if(response) // request completed
                {
                    Sk.loading.modal.remove();
                    try {
                        var json = $.parseJSON(response);
                        if(json) {

                            form.remove();
                            $('body',this.iframe.contents()).html('');
                            clearInterval(this.sendFilesInterval);

                            if(json.status!==true && json.status !== 1)
                            {
                                clearInterval(this.sendFilesInterval);
                                if('msg' in json) Sk.popup.simple.info({msg: json.msg});
                                this.sentFiles(false, json);
                            }
                            else
                            {
                                el2 = $('input:file', this.container).eq(this.sendFilesIndex);

                                el = $('<input type="hidden">');
                                var val = !el2.attr('data-store-path') ? json.data.filename : json.data.filepath;
                                el.attr('data-file','1').attr('name',el2.attr('data-name')).val(val).prependTo(this.container);
                                
                                clearInterval(this.sendFilesInterval);
                                this.sendFile(this.container, ++this.sendFilesIndex);
                            }
                        }
                    } catch(e) { // internal fail
                        clearInterval(this.sendFilesInterval);
                        Sk.popup.simple.info({msg: 'Falha interna'});
                        this.sentFiles(false);
                    }
                }
            }, this), form),1000);
        };

        /**
         * Conclui envio de arquivos
         * @access static
         * @return void
         */
        this.sentFiles = function(status, json)
        {
            $('form#'+inum).remove();
            $('iframe#httpsend').remove();
            this.sendFilesIndex = null;

//            if(status && this.sendFilesCallback)
            this.sendFilesCallback(status, json);
        };
    };
    
    
    Sk.form.ajaxFiles = SkFormAjaxFiles;
    

    /*
     * Utility for show content into popups
     */
    Sk.popup = {
        init: function () {
        },
        /**
         * Default useful events
         */
        events: {
            apply: function (parent)
            {
                var container = parent ? $(parent) : $('body');

                container.delegate('.modal-link', 'click', function (e)
                {
                    Sk.preventDefault(e);
                    Sk.popup.show($(this));
                });

                $('.modal-image', container).addClass('hand-cursor');
                $('.modal-html', container).addClass('hand-cursor');
                container.delegate('.modal-image', 'click', function (e)
                {
                    var img = $(this).data('image');
                    Sk.preventDefault(e);

                    if (img)
                        Sk.popup.simple.image({img: Sk.absolutePath + img});
                });
                container.delegate('.modal-info', 'click', function (e)
                {
                    Sk.popup.simple.info($(this));
                });
                container.delegate('.modal-html', 'click', function (e)
                {
                    var file = $(this).data('file');
                    Sk.preventDefault(e);

                    if (file) {
                        $.get(Sk.absolutePath + file, function (response) {
                            Sk.popup.simple.html({html: response});
                        });
                    } else {
                        Sk.popup.simple.html($(this));
                    }
                });
                container.delegate('.popup-confirm', 'click', function (e)
                {
                    Sk.preventDefault(e);
                    Sk.popup.simple.confirm($(this));
                });
            }
        },
        /**
         * Show popup according to data sent
         * @param jQuery obj any element or json data
         */
        show: function (obj)
        {
            'originalEvent' in obj ? obj = $(this) : null;
            var self = Sk.popup,
                    type = obj.data ? obj.data('type') : obj.type;

            switch (type)
            {
                case 'form':
                    self.form.show(obj);
                    break;
                default:
                    self.simple.info(obj);
                    break;
            }
        },
        /**
         * Utilities to use Forms into Popup
         */
        form: {
            /**
             * Default events for forms into popup
             */
            events: function (modal, obj)
            {
                var self = Sk.popup,
                        proxy = self.proxies[self.proxy],
                        objData = obj.data ? obj.data() : obj,
                        footer = proxy.getSlice(modal, 'footer');

                $('[type=submit]', footer).click(function (e) {
                    Sk.preventDefault(e);
                    $('form', $(this).parents('.modal:first')).submit();
                });
                $('.modal-close', footer).click(function (e) {
                    Sk.preventDefault(e);
                    Sk.popup.close(this, false);
                });

                if (objData.callback) {
                    if (typeof objData.callback === 'string') {
                        objData.callback = eval(objData.callback);
                    }
                    if (typeof objData.callback === 'function') {
                        objData.callback(modal);
                    }
                }

                proxy.clickBsModal(modal, function () {
                    Sk.popup.close($(this), false);
                });

                $('.datatable, .grid, .form, .view', modal).trigger('loaded');
            },
            /**
             * Show a popup with form loaded
             * @param jQuery obj any element or json data with settings for set the popup
             */
            show: function (obj)
            {
                var objData = obj, url, sendData = {}, id, params, type = 'GET';
                if (obj.data)
                {
                    objData = obj.data();
                }

                url = objData.url || obj.attr('href');
                id = !obj.modalId ? url.replace(Sk.absolutePath, '').replace(/\//g, '-') : obj.modalId;
                if (obj.params) {
                    url = url + "?" + obj.params;
                }

                if ('sendData' in objData) {
                    sendData = objData.sendData;
                    type = 'POST';
                }

                Sk.loading.modal.add();
                $.ajax({
                    url: url,
                    data: sendData,
                    type: type,
                    success: function (response)
                    {
                        var self = Sk.popup,
                                proxy = self.proxies[self.proxy],
                                header, actions, data, modal, width;
                        response = $(response);

                        header = $('header h3, header h4, header h5, .header h3, .header h4, .header h5', response).eq(0);
                        actions = $('.actions:not(.action-filters):last', response);

                        $('header, .header', response).eq(0).remove();
                        $('.actions:not(.action-filters):last', response).remove();

                        data = {};
                        data.id = id;
                        data.body = response;

                        data.header = ('header' in obj) ? obj.header : header;
                        data.footer = ('footer' in obj) ? obj.footer : actions;

                        data.width = !!(width = response.width()) ? width : $(window).width() - proxy.sidesMargin;
                        modal = Sk.popup.simple.show(data);

                        proxy.onShown(modal);
                        /* events */
                        Sk.popup.form.events(modal, obj);
                        Sk.loading.modal.remove();
                    },
                    error: Sk.request.exception.ajaxError
                });
            }
        },
        /**
         * Utilities to show Content into Popup
         */
        simple: {
            /**
             * Default events for content into popup
             */
            events: function (modal) {
                $('.modal-close', modal).click(function (e) {
                    Sk.preventDefault(e);
                    Sk.popup.close(this, false);
                });
            },
            /**
             * Show content into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            show: function (data)
            {
                var self = Sk.popup,
                        proxy = self.proxies[self.proxy],
                        id = data.id || _.uniqueId('r'),
                        html = Sk.data.popup.html.proxies[self.proxy].div.replace('{id}', id),
                        x, modal;

                modal = $(html);

                for (x in data) {
                    if ($.inArray(x, ['id', 'width']) !== -1) {
                        continue;
                    }

                    if (data[x] === false) {
                        proxy.getSlice(modal, x).remove();
                    } else {
                        proxy.getSlice(modal, x).prepend(data[x]);
                    }
                }

                if (data.width) {
                    proxy.setWidth(modal, data.width);
                }

                proxy.show(modal, data.sets || {});
                self.simple.events(modal);

                return modal;
            },
            /**
             * Show image into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            image: function (obj)
            {
                var self = Sk.popup,
                        data = {},
                        objData = obj.data ? obj.data() : obj,
                        modal;

                data.header = ('header' in objData ? objData.header : ('title' in objData ? objData.title : Sk.data.popup.msg.imageViewTitle));
                data.body = Sk.data.popup.html.proxies[Sk.popup.proxy].img.replace('{img}', objData.img).replace('{msg}', ('msg' in objData ? objData.msg : ''));
                data.footer = Sk.data.popup.html.proxies[Sk.popup.proxy].close.replace('{close}', Sk.data.popup.msg.close);
                if ('id' in objData) {
                    data.id = objData.id;
                }

                modal = Sk.popup.simple.show(data);
                modal.addClass('info').addClass('image');

                return modal;
            },
            /**
             * Show html into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            html: function (obj)
            {
                var data = {}, objData = obj, modal;
                if (obj.data)
                {
                    objData = obj.data();
                }

                data.header = ('header' in objData ? objData.header : ('title' in objData ? objData.title : Sk.data.popup.msg.htmlViewTitle));
                data.body = Sk.data.popup.html.proxies[Sk.popup.proxy].html.replace('{html}', objData.html);
                data.footer = ('footer' in objData ? objData.footer : Sk.data.popup.html.proxies[Sk.popup.proxy].close);
                typeof data.footer === 'string' && data.footer.replace('{close}', Sk.data.popup.msg.close);
                
                if ('id' in objData) {
                    data.id = objData.id;
                }

                data.width = !('width' in objData) ? 1000 : parseInt(objData.width,10);
                modal = Sk.popup.simple.show(data);

                modal.addClass('info').addClass('html');

                return modal;
            },
            /**
             * Show message into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            info: function (obj)
            {
                var data = {}, objData = obj, modal;
                if (obj.data)
                {
                    objData = obj.data();
                }

                data.header = ('header' in objData && objData.header !== "" ? objData.header : 
                        ('title' in objData && objData.title !== "" ? objData.title : Sk.data.popup.msg.infoViewTitle));
                data.body = ('body' in objData && objData.body !== "" ? objData.body : 
                        ('msg' in objData && objData.msg !== "" ? objData.msg : ''));
                data.footer = Sk.data.popup.html.proxies[Sk.popup.proxy].ok.replace('{ok}', Sk.data.popup.msg.ok);
                if ('id' in objData) {
                    data.id = objData.id;
                }
                data.sets = 'sets' in objData ? objData.sets : {};

                modal = Sk.popup.simple.show(data);
                modal.addClass('info').addClass('message');

                return modal;
            },
            /**
             * Show a confirmation popup with a message
             * @param jQuery obj any element or json data with settings for set the popup
             */
            confirm: function (obj)
            {
                var data = {}, modal, objData = (typeof obj.data === 'function') ? obj.data() : obj;

                data.width = objData.width || null;
                data.header = objData.title || Sk.data.popup.msg.confirmViewTitle;
                data.body = objData.msg || Sk.data.popup.msg.confirmViewMsg;
                data.footer = Sk.data.popup.html.proxies[Sk.popup.proxy].confirmActions;
                data.sets = 'sets' in objData ? objData.sets : {};

                modal = Sk.popup.simple.show(data);
                objData.modal = modal;
                $('.modal-footer .confirm', modal).html(objData.confirm || Sk.data.popup.msg.confirmActionMsg).addClass(objData.confirmClass || 'btn-danger');
                $('.modal-footer .cancel', modal).html(objData.dataCancel || Sk.data.popup.msg.cancelActionMsg);
                $('.modal-footer .confirm, .modal-footer .cancel', modal).click(_.partial(function (objData, e) {
                    Sk.preventDefault(e);
                    var status = true, url;
                    if ($(this).hasClass('cancel')) {
                        status = false;
                    }

                    !$(this).hasClass('keepopened') && Sk.popup.close($(this));
                    if (!objData.callback) {
                        url = (obj.url || (obj.data && obj.data('url'))) ? obj.url || obj.data('url') : null;
                        if (status && url) {
                            Sk.content.get(obj);
                        }
                    }

                    if (typeof objData.callback === 'string') {
                        objData.callback = eval(objData.callback);
                    }
                    if (typeof objData.callback === 'function') {
                        objData.callback(status, objData);
                    }
                }, objData));

                modal.addClass('confirm').addClass('info');

                return modal;
            },
            /**
             * Show confirmation popup with a message and text field
             * @param jQuery obj any element or json data with settings for set the popup
             */
            confirmMessage: function (obj)
            {
                var data = {}, modal, objData = obj;
                if (typeof obj.data === 'function') {
                    objData = obj.data();
                }


                data.header = objData.title || Sk.data.popup.msg.confirmViewTitle;
                data.bodyTpl = objData.body || Sk.data.popup.html.proxies[Sk.popup.proxy].confirmMessage;
                data.body = data.bodyTpl.replace('{msg}', objData.msg || Sk.data.popup.msg.confirmViewMsg).replace('{val}', '');
                data.footer = Sk.data.popup.html.proxies[Sk.popup.proxy].confirmActions;
                ('width' in objData) && (data.width = objData.width);

                modal = this.show(data);
                $('textarea', modal).val(objData.val);
                $('.modal-footer .confirm', modal).html(objData.confirm || Sk.data.popup.msg.confirmActionMsg).addClass(objData.confirmClass || 'btn-danger');
                $('.modal-footer .cancel', modal).html(objData.dataCancel || Sk.data.popup.msg.cancelActionMsg);
                $('.modal-footer .confirm, .modal-footer .cancel', modal).click(_.partial(function (modal, e) {
                    Sk.preventDefault(e);
                    var status = true,
                            $confirmMessage = $('#modal-confirm-message'),
                            msg = $confirmMessage.is('textarea') ? $confirmMessage.val() : ($confirmMessage.is('input') ? $confirmMessage.val() : $('option:selected', $confirmMessage).val());
                    if ($(this).hasClass('cancel')) {
                        status = false;
                    }

                    if ((!modal.attr('data-opt-msg') && !msg) && status) {
                        return Sk.popup.simple.info({msg: Sk.data.popup.msg.confirmValidation});
                    }


                    Sk.popup.close($(this));
                    if (!objData.callback) {
                        return Sk.popup.form.remove(status, obj);
                    }

                    if (typeof objData.callback === 'string') {
                        objData.callback = eval(objData.callback);
                    }
                    if (typeof objData.callback === 'function') {
                        objData.callback(msg, status, objData);
                    }
                }, modal));

                modal.addClass('confirm').addClass('message');

                return modal;
            }
        },
        /**
         * Close a popup
         * @param jQuery obj any element into the popup
         */
        close: function (obj)
        {
            var self = Sk.popup, modal;

            obj = !obj ? $(this) : $(obj);
            modal = self.proxies[self.proxy].getByReference(obj);

            return self.proxies[self.proxy].close(modal);
        }
    };

    Sk.popup.proxy = 'bootstrap4';
    Sk.popup.proxies = {};
    Sk.popup.proxies.bootstrap3 = {
        sidesMargin: 70,
        getByReference: function (reference) {
            return reference.hasClass('modal') ? reference : reference.parents('.modal');
        },
        close: function (modal) {
            return $('.modal-header button.close', modal).trigger('click');
        },
        getSlice: function (modal, slice) {
            return $('.modal-' + slice, modal);
        },
        setWidth: function (modal, width) {
            modal.css('width', width + this.sidesMargin).css('max-width', '100%').css('margin', 'auto');

            $('.modal-dialog', modal).css('width', 'auto');
        },
        show: function (modal, sets) {
            var x;
            for(x in sets) {
                modal.attr('data-'+x, sets[x]);
            }
            modal.modal('show')
                    .on('hidden.bs.modal', function () {
                        $(this).remove();
                    })
                    .on('shown.bs.modal', function () {
                        $('.modal-footer .btn', $(this)).eq(0).focus();
                    });
        },
        clickBsModal: function (modal, callback) {
            modal.on('hidden.bs.modal', callback);
        },
        onShown: function (modal) {

            modal.on('shown.bs.modal', function () {
                var bodyHeight = $(window).innerHeight() - (parseInt($('.modal-dialog', modal).css('margin-top').replace('px', ''), 10) + parseInt($('.modal-dialog', modal).css('margin-bottom').replace('px', ''), 10) + parseInt($('.modal-header', modal).css('height').replace('px', ''), 10) + parseInt($('.modal-footer', modal).css('height').replace('px', ''), 10));
                $('.modal-body', modal).css('max-height', bodyHeight + 'px');

                Sk.events.apply(modal);
            });
        }
    };
    
    Sk.popup.proxies.bootstrap4 = {
        sidesMargin: 70,
        getByReference: function (reference) {
            return reference.hasClass('modal') ? reference : reference.parents('.modal');
        },
        close: function (modal) {
            modal.removeClass('show');
            modal.prev().filter('.modal-backdrop').removeClass('show');
            setTimeout(_.partial(function(modal){
                modal.trigger('hidden.bs.modal');
            }, modal), 200);
            return true;
        },
        getSlice: function (modal, slice) {
            return $('.modal-' + slice, modal);
        },
        setWidth: function (modal, width) {
            if(_.indexOf(['lg', 'sm', 'full'], width)>-1) {
                $('.modal-dialog', modal).addClass('modal-'+width);
                return;
            }
            modal.css('width', width + this.sidesMargin).css('max-width', '100%').css('margin', 'auto');

            $('.modal-dialog', modal).css('width', 'auto');
        },
        show: function (modal, sets) {
            var x;
            for(x in sets) {
                modal.attr('data-'+x, sets[x]);
            }
            
            modal.modal('show');
            this.setZIndexes(modal);
                    
            modal.addClass('show')
                    .on('hidden.bs.modal', function () {
                        var modal = this;
                        $(modal).prev().filter('.modal-backdrop').remove();
                        $(modal).remove();
                        
                        !$('body > .modal').length && $('.modal-backdrop').remove(); // to ensure all backdrops were removed
                    })
                    .on('shown.bs.modal', function () {
//                        console.log(parseInt($(this).css('z-index'), 10)-1);
                        $('.modal-footer .btn', $(this)).eq(0).focus();
                    });
        },
        setZIndexes: function (modal) {
            var lastModal = $('.modal.show:last');
            if(lastModal.length) {
                var currentZIndex = parseInt($('.modal.show:last').css('z-index'), 10);
                modal.css('z-index', currentZIndex+2);
                $('.modal-backdrop:last').css('z-index', currentZIndex+1);
            }
        },
        clickBsModal: function (modal, callback) {
            modal.on('hidden.bs.modal', callback);
        },
        onShown: function (modal) {

            modal.on('shown.bs.modal', function () {
                var bodyHeight = $(window).innerHeight() - (parseInt($('.modal-dialog', modal).css('margin-top').replace('px', ''), 10) + parseInt($('.modal-dialog', modal).css('margin-bottom').replace('px', ''), 10) + parseInt($('.modal-header', modal).css('height').replace('px', ''), 10) + parseInt($('.modal-footer', modal).css('height').replace('px', ''), 10));
                $('.modal-body', modal).css('max-height', bodyHeight + 'px');

                Sk.events.apply(modal);
            });
        }
    };

    // Utility Functions
    // -----------------

    /**
     * Prevent default event
     * @param e event
     */
    Sk.preventDefault = function (e)
    {
        if (!e)
            return;

        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    };

    /**
     * Prevent propagation
     * @param e event
     */
    Sk.stopAllEvents = function (e)
    {
        e.stopPropagation();
        this.preventDefault(e);
    };

    /**
     * Wait for a function return to be true before executes another function
     * @param waitFunction function
     * @param afterWait function
     * @param interval integer microseconds interval between checks
     */
    Sk.waitFor = function (waitFunction, afterWait, interval) {
        var test = waitFunction();
        !interval && (interval = 200);

        if (!test) {
            return _.delay(_.bind(function (waitFunction, afterWait, interval) {
                Sk.waitFor(waitFunction, afterWait, interval);
            }, this, waitFunction, afterWait, interval), interval);
        }

        afterWait();
    };

    /**
     * Default useful events collection
     */
    Sk.events.apply = function (container) {
        Sk.content.events.apply(container);
        Sk.popup.events.apply(container);
    };

    /**
     * Implements ajax navigation into browser back and foward buttons
     */
    root.onpopstate = function (e)
    {
        var state = e.state;
        if (Sk.AllowSpa && typeof state === 'object' && state && 'page' in state && 'sk' in state) {
            Sk.content.load({url: state.page});
        }
    };

    return Sk;
}));
